import { View, Text } from 'react-native'
import React from 'react'
import { Stack } from 'expo-router'

export const unstable_settings = {
  // Ensure that reloading on `/modal` keeps a back button present.
  initialRouteName: '(tabs)/stacks',
};


const stacksLayout = () => {
  return (
    <Stack>
      <Stack.Screen name="index" options={{ headerShown: false }} />
      <Stack.Screen name="cupboard1" />
      <Stack.Screen name="cupboard2" />
    </Stack>
  )
}

export default stacksLayout