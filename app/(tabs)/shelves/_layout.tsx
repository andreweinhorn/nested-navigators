import { View, Text } from 'react-native'
import React from 'react'
import { Stack } from 'expo-router'

export const unstable_settings = {
  // Ensure that reloading on `/modal` keeps a back button present.
  initialRouteName: '(tabs)/shelves',
};

const shelvesLayout = () => {

  return (
    <Stack>
      <Stack.Screen name="index" options={{ headerShown: false }} />
      <Stack.Screen name="shelf1" />
      <Stack.Screen name="shelf2" />
    </Stack>
  )
}

export default shelvesLayout