import { View, Text, Pressable } from 'react-native'
import React from 'react'
import { useRouter } from 'expo-router'

const horsey = () => {
  const router = useRouter()

  return (
    <View>
      <Text>shelves</Text>
      <Pressable onPress={() => router.push('/(tabs)/shelves/shelf1')}>
        <Text>
          Go to Shelf 1
        </Text>
      </Pressable>
      <Pressable onPress={() => router.push('/(tabs)/shelves/shelf2')}>
        <Text>
          Go to Shelf 2
        </Text>
      </Pressable>
    </View>
  )
}

export default horsey